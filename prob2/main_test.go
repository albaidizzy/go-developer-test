package prob2

import (
	"fmt"
	"testing"

	"github.com/magiconair/properties/assert"
)

func Test_Quicksort(t *testing.T) {
	var a = []int{0, 1, 2, 2, 3, 1, 0, 0, 2, 0, 1, 1, 0}
	s := Quicksort(a)
	fmt.Println(s)
	var expected = []int{0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3}
	assert.Equal(t, a, expected)
}
