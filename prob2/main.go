package prob2

import (
	"fmt"
	"math/rand"
)

func main() {
	slice := []int{0, 1, 2, 2, 3, 1, 0, 0, 2, 0, 1, 1, 0}
	fmt.Println("\n--- Unsorted --- \n\n", slice)
	Quicksort(slice)
	fmt.Print(slice)
}

func Quicksort(a []int) []int {
	if len(a) < 2 {
		return a
	}

	left, right := 0, len(a)-1

	pivot := rand.Int() % len(a)

	a[pivot], a[right] = a[right], a[pivot]

	for i, _ := range a {
		if a[i] < a[right] {
			a[left], a[i] = a[i], a[left]
			left++
		}
	}

	a[left], a[right] = a[right], a[left]

	Quicksort(a[:left])
	Quicksort(a[left+1:])

	return a
}
