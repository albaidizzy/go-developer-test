# Go Developer Test

This test is intended for prospective Go Developers to join PT Lussa Teknologi Global. You need to do this test first before joining our team. Happy Coding and Good Luck!

## How To

- Fork this repositories into your own
- Your time spent to solve this problem are counted as your coding speed performance, faster you complete the test, then bigger the score
- You can write your solution using this tools https://play.golang.org/ or you can use installed Golang on your local machine instead
- Follow the rules and answer the Tests below
- Email your result (forked repository URL) to **[web.jobs@lussa.net](mailto:web.jobs@lussa.net)** with following email subject `GODEV - [Full Name] - [WA Contact]`. Example : `GODEV - Lussa Alien - 0811 234 5678`

## Test 1

This test is a general coding test to check your algorithm skill

### Rules

- Write your solutions in test1 directory (create a new folder named `test1`)
- Solution files should be using this formats `<problem_number>.go`
- Write Readme (Markdown) for how to execute your code
- **No library allowed outside Go Standard Package/Library**

### Problems 

1. Given a limited range of size `n` where array contains elements between `1` to `n-1` with one element repeating, find the duplicate number in it.

    **(5 points)**

    examples:
    ```javascript
    [1,2,3,4,4]
    the duplicate element is 4
    ```
    
    ```javascript
    [1,2,3,4,2]
    the duplicate element is 2
    ```

1. Given an array containing only `0`'s, `1`'s and `2`'s. sort the array in linear time and using constant space

    **(6 points)**

    examples:
    ```javascript
    [0,1,2,2,1,0,0,2,0,1,1,0]
    [0,0,0,0,0,1,1,1,1,2,2,2]
    ```

1. Given an array of integers find all distinct combinations of given length where repetition of elements is allowed

    **(6 points)**

    examples:
    ```javascript
    [1,2,3]
    [[1,1], [1,2], [1,3], [2,2], [2,3], [3,3]]
    ```

1. Given an array of integers, find maximum product (multiplication) subarray. In other words, find sub-array that has maximum product of its elements

    **(10 points)**

    examples:
    ```javascript
    [-6,4,-5,8,-10,0,8]
    the maximum product sub-array is [4,-5,8,-10] having product 1600
    ```

## Test 2

This test is a coding test to check your backend development skill

### Rules

- Write your solutions in test2 directory (create a new folder named `test2`)

### Problems 

- Create an API Service using Go
- You are free to make the app you want to make. Example : Movie API, Weather API, Postal Code API, etc
- The API service that you created must be able to be consumed at least by Postman or Altair
- Use your own directory structure/project layout or use Golang Standard Layout (Recommended)
- Use Go Modules
- Write proper documentation and readme for your code, example : how to install, how to run, how to consume, etc
- More Complex your Apps, bigger score you get
- You can use library/package that enhance your app such as Viper, Cobra, Gqlgen, etc



## Questions?

Contact your employer for any question assistance