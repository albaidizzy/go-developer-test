package prob1

import (
	"testing"

	"github.com/magiconair/properties/assert"
)

func Test_DuplicateInArray(t *testing.T) {
	dup1 := DuplicateInArray([]int{1, 2, 3, 4, 4})
	assert.Equal(t, dup1, 4)

	dup2 := DuplicateInArray([]int{1, 2, 3, 4, 2})
	assert.Equal(t, dup2, 2)
}
