package routes

import (
	"time"

	"apitest/infrastructures"
	"apitest/infrastructures/adapter"
	"apitest/interfaces"

	"github.com/gorilla/mux"
	config "github.com/spf13/viper"
	"github.com/urfave/negroni"
)

type Route struct {
	Database       interfaces.ISQL
	Redis          interfaces.IRedis
	MySQLConnector adapter.MySQLAdapter
}

func (r *Route) Init() *mux.Router {

	//Initialize controller
	healthController := r.InitHealthController()
	userController := r.InitUserHander()
	authenticator := r.InitAuthenticator()

	router := mux.NewRouter().StrictSlash(false)
	all := router.PathPrefix("/v1").Subrouter()

	authRoute := mux.NewRouter().PathPrefix("/v1/auth").Subrouter()
	router.PathPrefix("/v1/auth").Handler(negroni.New(
		negroni.HandlerFunc(authenticator.Authenticate),
		negroni.Wrap(authRoute),
	))

	// auth := authRoute.PathPrefix("/v1/auth").Subrouter()

	authRoute.HandleFunc("/user", userController.GetUserByUsername).Methods("GET")
	authRoute.HandleFunc("/user/logout", userController.Logout).Methods("POST")
	all.HandleFunc("/user/register", userController.Register).Methods("POST")
	all.HandleFunc("/user/login", userController.AuthenticatUser).Methods("POST")

	// Health
	all.HandleFunc("/health/status", healthController.GetStatus).Methods("GET")
	all.HandleFunc("/health/liveness", healthController.GetStatus).Methods("GET")
	all.HandleFunc("/health/readiness", healthController.Readiness).Methods("GET")

	return router
}

// InitSQL set the sql values
func InitSQL() *infrastructures.SQLInfrastructure {
	sqlInfra := new(infrastructures.SQLInfrastructure)

	sqlInfra.SQLSlave.User = config.GetString("database_sql_slave.user")
	sqlInfra.SQLSlave.Password = config.GetString("database_sql_slave.password")
	sqlInfra.SQLSlave.Host = config.GetString("database_sql_slave.host")
	sqlInfra.SQLSlave.Port = config.GetInt("database_sql_slave.port")
	sqlInfra.SQLSlave.DBName = config.GetString("database_sql_slave.db_name")
	sqlInfra.SQLSlave.Charset = config.GetString("database_sql_slave.charset")

	sqlInfra.SQLMaster.User = config.GetString("database_sql_master.user")
	sqlInfra.SQLMaster.Password = config.GetString("database_sql_master.password")
	sqlInfra.SQLMaster.Host = config.GetString("database_sql_master.host")
	sqlInfra.SQLMaster.Port = config.GetInt("database_sql_master.port")
	sqlInfra.SQLMaster.DBName = config.GetString("database_sql_master.db_name")
	sqlInfra.SQLMaster.Charset = config.GetString("database_sql_master.charset")

	sqlInfra.OpenConnection()
	sqlInfra.SetConnMaxLifetime(config.GetDuration("database.max_life_time") * time.Second)
	sqlInfra.SetMaxIdleConns(config.GetInt("database.max_idle_connection"))
	sqlInfra.SetMaxOpenConns(config.GetInt("database.max_open_connection"))

	return sqlInfra
}
