module apitest

go 1.14

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/mux v1.8.0
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.7.1
	github.com/urfave/cli v1.22.5
	github.com/urfave/negroni v1.0.0
	golang.org/x/crypto v0.0.0-20210415154028-4f45737414dc
)
