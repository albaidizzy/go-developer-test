package interfaces

type IEncrypt interface {
	EncryptPassword()
}
