package prob3

import (
	"testing"

	"github.com/magiconair/properties/assert"
)

func Test_ArrayCombination(t *testing.T) {
	got := ArrayCombination([]int{1, 2, 3})
	expect := map[int][]int{0: {1, 1}, 1: {1, 2}, 2: {1, 3}, 3: {2, 2}, 4: {2, 3}, 5: {3, 3}}
	assert.Equal(t, got, expect)
}
