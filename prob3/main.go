package prob3

func ArrayCombination(arr []int) map[int][]int {
	visited := make(map[int][]int, 10)
	idx := 0
	for i := 0; i < len(arr); i++ {

		visited[idx] = []int{arr[i], arr[i]}
		idx++
		if i < len(arr)-1 {
			for j := i + 1; j < len(arr); j++ {
				visited[idx] = []int{arr[i], arr[j]}
				idx++
			}
		}
	}
	return visited
}
